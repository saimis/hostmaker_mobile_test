const settings = {
  api_url: 'http://hostmaker-test.herokuapp.com/api/v1/',
  api_owndoms_url: 'http://hostmaker-test.herokuapp.com/api/v1/owndoms.json',
  google_maps_url: 'http://maps.apple.com/?daddr=',
  map_zoom: 0.1,
  default_geo_center: { lat: 51.507383, lng: -0.127714 }
}

export default settings
