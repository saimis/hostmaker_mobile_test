import { StackNavigator } from 'react-navigation'

import List from '../screens/Owndoms/List'
import Details from '../screens/Owndoms/Details'

export const OwndomsStack = StackNavigator({
  List: {
    screen: List,
  },
  Details: {
    screen: Details,
  },
})
