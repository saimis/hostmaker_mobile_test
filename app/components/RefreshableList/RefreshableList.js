import React from 'react'
import { ListView, RefreshControl } from 'react-native'
import styles from './styles'

const data_source = new ListView.DataSource({rowHasChanged: (current_row, next_row) => current_row !== next_row})

export default class RefreshableList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      dataSource: data_source.cloneWithRows(this.props.list),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.list !== nextProps.list) {
      this.setState({
        dataSource: data_source.cloneWithRows(nextProps.list),
      })
    }
  }

  render() {
    const Row = this.props.row

    return (
      <ListView
        style={styles.refreshableList}
        refreshControl={
          <RefreshControl
            refreshing={this.props.refreshing}
            onRefresh={this.props.onrefresh}
          />
        }
        dataSource={this.state.dataSource}
        renderRow={(data) => <Row {...data} navigate={this.props.navigate} />}
      />
    )
  }
}

RefreshableList.propTypes = {
  list:       React.PropTypes.array,
  refreshing: React.PropTypes.bool,
  onrefresh:  React.PropTypes.func,
}

RefreshableList.defaultProps = {
  list:      [],
  refreshing: false,
  onrefresh: () => console.log('List refreshed'),
}
