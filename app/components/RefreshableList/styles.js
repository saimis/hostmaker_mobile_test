import { StyleSheet } from 'react-native'
import { colors } from '../../config/styles'

export default StyleSheet.create({
  refreshableList: {
    flex: 1,
    backgroundColor: colors.background,
    alignSelf: 'stretch',
  }
})
