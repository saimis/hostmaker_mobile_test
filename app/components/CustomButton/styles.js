import { StyleSheet } from 'react-native'
import { colors } from '../../config/styles'

export default StyleSheet.create({
  customButton: {
    backgroundColor: colors.customButtonBackground,
    borderRadius: 5,
    margin: 5,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  text: {
    alignSelf: 'center',
    color: colors.customButtonText,
    fontSize: 16,
    fontWeight: '500',
  },
})
