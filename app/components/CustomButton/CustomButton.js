import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import styles from './styles'

const CustomButton = (props) => {
  return (
    <TouchableOpacity style={styles.customButton} onPress={props.onPress}>
      <Text style={styles.text}>
        {props.text}
      </Text>
    </TouchableOpacity>
  )
}

CustomButton.propTypes = {
  text:    React.PropTypes.string,
  onPress: React.PropTypes.func,
}

CustomButton.defaultProps = {
  text:    'Custom button',
  onPress: () => console.log('Custom button pressed'),
}

export default CustomButton
