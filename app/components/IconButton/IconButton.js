import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import styles from './styles'
import missing_icon from '../../images/missing_ico.png'

const IconButton = (props) => {
  return (
    <TouchableOpacity style={styles.iconButton} onPress={props.onPress}>
      <Image source={props.icon} />
    </TouchableOpacity>
  )
}

IconButton.propTypes = {
  icon:    React.PropTypes.number,
  onPress: React.PropTypes.func,
}

IconButton.defaultProps = {
  icon:    missing_icon,
  onPress: () => console.log('Icon button pressed'),
}

export default IconButton
