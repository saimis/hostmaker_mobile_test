import { StyleSheet } from 'react-native'
import { colors } from '../../config/styles'

export default StyleSheet.create({
  iconButton: {
    paddingRight: 20,
  },
})
