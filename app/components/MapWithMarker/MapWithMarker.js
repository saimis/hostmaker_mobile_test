import React from 'react'
import { View } from 'react-native'
import MapView from 'react-native-maps'
import styles from './styles'
import settings from '../../config/settings'

const MapWithMarker = (props) => {
  const lat = parseFloat(props.lat)
  const lng = parseFloat(props.lng)

  return (
    <View style={styles.container}>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: lat,
          longitude: lng,
          latitudeDelta: settings.map_zoom,
          longitudeDelta: settings.map_zoom,
        }}
      >
        <MapView.Marker
          coordinate={{ latitude: lat, longitude: lng }}
        />
      </MapView>
    </View>
  )
}

MapWithMarker.propTypes = {
  lat: React.PropTypes.string,
  lng: React.PropTypes.string,
}

MapWithMarker.defaultProps = {
  lat: settings.default_geo_center.lat,
  lng: settings.default_geo_center.lng,
}

export default MapWithMarker
