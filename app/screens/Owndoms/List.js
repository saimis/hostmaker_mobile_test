import React from 'react'
import {
  StyleSheet,
  View,
  AsyncStorage,
  NetInfo,
  ToastAndroid,
  ActivityIndicator
} from 'react-native'
import axios from 'axios'
import { colors } from '../../config/styles'
import settings from '../../config/settings'
import Row from './Row'
import RefreshableList from '../../components/RefreshableList'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.mainBackground,
    flex: 1,
    justifyContent: 'center',
  },
  headerStyle:{
    backgroundColor: colors.lightGreyBackground,
    height: 40,
  },
  headerTitleStyle: {
    alignSelf: 'center',
    color: colors.headerText,
    fontSize: 12,
  },
})

export default class OwndomsList extends React.Component {
  constructor(props) {
    super()

    this.state = {
      owndoms: [],
      isConnected: true,
      isRefreshing: false
    }
  }

  static navigationOptions = {
    title: 'Available properties',
    headerStyle: styles.headerStyle,
    headerTitleStyle: styles.headerTitleStyle,
  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', () => { this.onConnectivityChange } )
    NetInfo.isConnected.fetch().done((isConnected) => { this.setState({ isConnected }) } )

    this.loadData()
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', () => { this.onConnectivityChange } )
  }

  onConnectivityChange(isConnected) {
    this.setState({ isConnected })
  }

  loadData() {
    AsyncStorage.getItem(this.cacheKey(new Date())).then((response) => {
      if (response) {
        this.setState({ owndoms: JSON.parse(response) })
      } else {
        this.requestData()
      }
    }).catch((error) => { throw error })
  }

  requestData() {
    if (this.state.isConnected) {
      axios.get(settings.api_owndoms_url).then(res => {
        if (res.data) {
          this.setState({ owndoms: res.data, isRefreshing: false }, this.saveData(res.data))
        }
      })
    } else {
      this.setState({ isRefreshing: false }, ToastAndroid.show('No internet connection', ToastAndroid.SHORT))
    }
  }

  saveData(data) {
    AsyncStorage.setItem(this.cacheKey(new Date()), JSON.stringify(data))
    this.removeData()
  }

  removeData() {
    let date = new Date()
    date.setHours(date.getHours() - 1)
    AsyncStorage.removeItem(this.cacheKey(date))
  }

  navigateToOwndom() {
    const { navigate } = this.props.navigation
    navigate('Details')
  }

  cacheKey(date) {
    // cache key ex: 2017091012
    return date.toISOString().slice(0,13).replace(/-|T/g,'')
  }

  onListRefresh() {
    this.setState({isRefreshing: true})
    this.requestData()
  }

  render () {
    const { navigate } = this.props.navigation

    return (
      <View style={styles.container}>
        {this.state.owndoms.length > 0 ?
          <RefreshableList
            list={this.state.owndoms}
            refreshing={this.state.isRefreshing}
            onrefresh={() => { this.onListRefresh() }}
            row={Row}
            navigate={(id) => {
              navigate(
                'Details',
                { owndom: this.state.owndoms.find(o => o.id === id)}
              )
            }}
          /> :
          <ActivityIndicator size="large" />
        }
      </View>
    )
  }
}

OwndomsList.propTypes = {
  navigation: React.PropTypes.object,
}
