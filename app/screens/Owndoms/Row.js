import React from 'react'
import {
  TouchableHighlight,
  Text,
  StyleSheet
} from 'react-native'
import { colors } from '../../config/styles'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderTopColor: colors.mediumGreyBorder,
    borderTopWidth: 1,
    flex: 1,
    flexDirection: 'row',
    padding: 12,
  },
  text: {
    marginLeft: 20,
    fontSize: 16,
  },
})

const Row = (props) => (
  <TouchableHighlight style={styles.container} onPress={() => { props.navigate(props.id) }} underlayColor={colors.mediumGreyBorder}>
    <Text style={styles.text}>
     {props.owner} - {props.address.city} {props.income}£
    </Text>
  </TouchableHighlight>
)

export default Row
