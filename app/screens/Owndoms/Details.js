import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Linking,
  ToastAndroid,
  Image,
  TouchableHighlight
} from 'react-native'
import settings from '../../config/settings'
import { colors } from '../../config/styles'
import CustomButton from '../../components/CustomButton'
import IconButton from '../../components/IconButton'
import MapWithMarker from '../../components/MapWithMarker'
import information_icon from '../../images/information_ico/information_ico.png'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mainBackground,
  },
  headerContainer: {
    flex: 0.7,
    padding: 10
  },
  headerStyle:{
    height: 40,
    backgroundColor: colors.lightGreyBackground,
  },
  headerTitleStyle: {
    color: colors.headerText,
    fontFamily: 'monospace',
    fontSize: 12,
  },
  headerButton: {
    marginRight: 10
  },
  mapContainer: {
    flex: 1,
    borderTopWidth:1,
    borderTopColor: colors.mediumGreyBorder
  },
  descriptionContainer: {
    borderTopColor: colors.lightGreyBorder,
    borderTopWidth: 1,
    flex: 0.8,
    padding: 10
  },
  addressContainer: {
    flex: 1,
    padding: 10
  },
  actionContainer: {
    backgroundColor: colors.lightGreyBackground,
    paddingTop: 20,
    borderTopColor: colors.lightGreyBorder,
    borderTopWidth: 1
  },
  text: {
    color: colors.mainFontColor,
  },
  link: {
    color: colors.linkBlue,
    marginTop: 5,
    fontFamily: 'monospace',
    fontSize: 12
  },
  mapLink: {
    color: colors.linkBlue,
    marginTop: 15,
    fontFamily: 'monospace',
    fontSize: 12
  },
  headerTimeTitle: {
    fontSize: 35,
    color: colors.headerTimeTitleGreen,
    fontFamily: 'monospace'
  },
  headerTimeSubtitle: {
    fontSize: 10,
    color: colors.mainFontColor,
  },
  descriptionTitle: {
    color: colors.descriptionTitleGrey  ,
    fontFamily: 'monospace'
  },
})

export default class Details extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.owndom.owner}`,
    headerTitleStyle: styles.headerTitleStyle,
    headerStyle: styles.headerStyle,
    headerRight: (<IconButton icon={information_icon} onPress={navigation.state.params.handleHeaderButtonClick} />)
  })

  componentDidMount() {
    this.props.navigation.setParams({ handleHeaderButtonClick: this.onHeaderButtonPressed })
  }

  onStartNavigation() {
    Linking.openURL(
      `${settings.google_maps_url}${this.props.navigation.state.params.owndom.geo.lat},${this.props.navigation.state.params.owndom.geo.lng}`
    ).catch(err => console.error('An error occurred', err))
  }

  onReportDelay() {
    ToastAndroid.show('Report delay pressed', ToastAndroid.SHORT)
  }

  onPropertyEnter() {
    ToastAndroid.show('Enter property pressed', ToastAndroid.SHORT)
  }

  onHeaderButtonPressed() {
    ToastAndroid.show('Info button pressed', ToastAndroid.SHORT)
  }

  putRealTimeForRealism() {
    const date = new Date()
    const hours = date.getHours()

    return `${ hours % 12 || 12 }:${ ('0' + date.getMinutes()).slice(-2) } ${ hours >= 12 ? 'pm' : 'am' }`
  }

  render() {
    const { owndom } = this.props.navigation.state.params

    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerTimeTitle}>{this.putRealTimeForRealism()}</Text>

          <Text style={styles.headerTimeSubtitle}>Start of service time</Text>
          <Text style={styles.link} onPress={() => { this.onReportDelay() }}>REPORT DELAY</Text>
        </View>
        <View style={styles.mapContainer}>
          <MapWithMarker {...this.props.navigation.state.params.owndom.geo} />
        </View>
        <View style={styles.addressContainer}>
          <Text style={styles.text}>{owndom.address.address1}</Text>
          <Text style={styles.text}>
            {[owndom.address.address3, owndom.address.address2].filter(Boolean).join(' ')}
          </Text>
          <Text style={styles.text}>{owndom.address.address4}</Text>
          <Text style={styles.text}>{owndom.address.city} {owndom.address.postCode}</Text>
          <Text style={styles.mapLink} onPress={() => { this.onStartNavigation() }}>START NAVIGATION</Text>
        </View>
        <ScrollView style={styles.descriptionContainer}>
          <Text style={styles.descriptionTitle}>DIRECTIONS</Text>
          <Text style={styles.text}>{owndom.description}</Text>
        </ScrollView>
        <View style={styles.actionContainer}>
          <CustomButton text={'ENTER PROPERTY'} onPress={() => { this.onPropertyEnter() }}/>
        </View>
      </View>
    )
  }
}

Details.propTypes = {
  navigation: React.PropTypes.object,
}
